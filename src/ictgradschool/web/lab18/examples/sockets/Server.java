package ictgradschool.web.lab18.examples.sockets;

import ictgradschool.Keyboard;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 * Shows how to use a {@link java.net.ServerSocket} to create a {@link java.net.Socket}-based TCP server in Java.
 */
public class Server {

    private static final int PORT = 8181;

    private ServerSocket serverSocket;

    private void start() throws IOException {

        // Creates a new ServerSocket, listening on the given port.
        serverSocket = new ServerSocket(PORT);
        System.out.println("Created ServerSocket on port " + PORT);

        // Create a thread to handle server requests
        Thread thread = new Thread(serverTask, "ServerThread");
        thread.start();

        // Loop until the user hits ENTER, then quit the ServerSocket by calling close.
        // This will cause a SocketException to be thrown on the thread calling the accept() method.
        System.out.println("You may press ENTER to quit the server at any time.");
        Keyboard.readInput();
        serverSocket.close();

    }

    private final Runnable serverTask = new Runnable() {
        @Override
        public void run() {

            try {
                while (true) {

                    Socket client = null;
                    try {

                        System.out.println("Waiting for a client connection...");
                        client = serverSocket.accept();
                        System.out.println("A client connected!");

                        DataInputStream in = new DataInputStream(client.getInputStream());
                        int num1 = in.readInt();
                        int num2 = in.readInt();
                        System.out.println("Read from client: " + num1 + ", " + num2);

                        System.out.println("Performing complex maths...");
                        int result = num1 + num2;

                        System.out.print("Sending result back to client... ");
                        DataOutputStream out = new DataOutputStream(client.getOutputStream());
                        out.writeInt(result);
                        System.out.println("Done!");

                    } finally {
                        // Make sure to close the client connection when we're done with it.
                        if (client != null) {
                            client.close();
                            System.out.println("Connection to client closed.");
                        }
                    }
                }
            } catch (SocketException e) {
                // This Exception will be thrown when the user quits the server.
                System.out.println("Server closed.");

            } catch (IOException e) {
                // This Exception will be thrown if any other network error occurs.
                e.printStackTrace();
            }

        }
    };

    public static void main(String[] args) throws IOException {
        new Server().start();
    }
}
